/*
백준 12865번 문제
https://www.acmicpc.net/problem/12865
*/

import java.util.*;

public class KnapsackDP {
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);

        int N = sc.nextInt();
        int K = sc.nextInt();

        int[][] array = new int[N][2];
        int[][] cache = new int[N + 1][K + 1];

        for(int i = 0; i < N; i++)
        {
            array[i][0] = sc.nextInt();
            array[i][1] = sc.nextInt();
        }
        sc.close();

        System.out.println(Knapsack(N, K, array, cache));
    }

    static int Knapsack(int N, int K, int[][] array, int[][] cache)
    {
        for(int i = 1; i <= N; i++)
        {
            for(int j = 1; j <= K; j++)
            {
                if(array[i - 1][0] > j)
                {
                    cache[i][j] = cache[i-1][j];
                }
                else
                {
                    cache[i][j] = Math.max(array[i - 1][1] + cache[i-1][j-array[i - 1][0]], cache[i-1][j]);
                }
            }
        }
        return cache[N][K];
    }
}
