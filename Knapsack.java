/*
백준 12865번 문제
https://www.acmicpc.net/problem/12865
*/

import java.util.*;

public class Knapsack
{
    public static void main(String[] args)
    {
        Calculate calculate = new Calculate();
        System.out.println(calculate.StartCalculation());
    }

}

class Calculate
{
    int N;
    int K;
    int[] weights;
    int[] values;
    Scanner sc;

    int numOfCall;

    int answer;
    Calculate()
    {
        this.numOfCall = 0;
        this.sc = new Scanner(System.in);
        this.N = sc.nextInt();
        this.K = sc.nextInt();
        this.weights = new int[N];
        this.values = new int[N];

        for(int i = 0; i < this.N; i++)
        {
            this.weights[i] = sc.nextInt();
            this.values[i] = sc.nextInt();
        }
        sc.close();
    }

    public int StartCalculation()
    {
        ArrayList<Integer> list = new ArrayList<Integer>();
        int answer = GetCombinations(N, 0, 0, list);
        
        System.out.println("call : " + numOfCall);
        return answer;
    }

    int GetCombinations(int maxDepth, int currDepth, int index, ArrayList<Integer> combination)
    {
        numOfCall++;
        if(GetWeight(combination) > this.K)
        {
            return -1;
        }

        if(maxDepth <= currDepth)
        {
            return GetValue(combination);
        }

        if(index >= this.N)
        {
            return -1;
        }

        int zero = GetValue(combination);

        int first = GetCombinations(maxDepth, currDepth, index + 1, combination);

        combination.add(index);

        int second = GetCombinations(maxDepth, currDepth + 1, index + 1, combination);

        combination.remove(combination.size() - 1);
        return Math.max(zero, Math.max(first, second));
    }


    int GetValue(ArrayList<Integer> combinations)
    {
        int total = 0;
        for(int i = 0; i < combinations.size(); i++)
        {
            total += this.values[combinations.get(i)];
        }
        return total;
    }

    int GetWeight(ArrayList<Integer> combinations)
    {
        int total = 0;
        for(int i = 0; i < combinations.size(); i++)
        {
            total += this.weights[combinations.get(i)];
        }
        return total;
    }
}